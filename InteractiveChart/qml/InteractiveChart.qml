import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import QtCharts

// Page with example charts with dragable points
Page {
    id: page6
    title: qsTr("Page 6")

    // chart example with basic mouse handling
    Rectangle {
        id: chartRect
        width: parent.width
        height: 400

        ChartView {
            id: chartView
            title: "ChartView example"
            anchors.fill: parent
            antialiasing: true

            property var selectedPoint: undefined
            property int selectedIndex: -1

            property real toleranceX: 0.05
            property real toleranceY: 0.05

            LineSeries {
                id: lineSeries
                name: "Example lines"
                width: 3

                pointsVisible: true
                capStyle: Qt.SquareCap

                pointLabelsColor: "green"
                pointLabelsVisible: false

                selectedColor: "red"

                // line clicked...do something useful
                // not working if MouseArea is added
                onClicked: (point) => {
                   console.log("clicked " + point);
                }

                XYPoint { x: 0; y: 0.0 }
                XYPoint { x: 1.1; y: 3.2}
                XYPoint { x: 1.9; y: 2.4 }
                XYPoint { x: 2.1; y: 2.1 }
                XYPoint { x: 3.4; y: 2.3 }
                XYPoint { x: 4.1; y: 3.1 }
            }

            LineSeries {
                id: lineSeriesSelected
                name: "Selected lines"
                width: 5

                color: "red"
                pointsVisible: true
                capStyle: Qt.SquareCap

                pointLabelsColor: "red"
                pointLabelsVisible: true

                selectedColor: "red"
            }

            MouseArea {
                anchors.fill: parent
                onPressed: (mouse) => {
                    chartView.setSelected(mouse);
                }

                onPositionChanged: (mouse) => {
                    if(chartView.selectedPoint != undefined) {
                        var p = Qt.point(mouse.x, mouse.y);
                        var cp = chartView.mapToValue(p);
                        console.log("move point: " + mouse);

                        lineSeries.replace(chartView.selectedPoint.x, chartView.selectedPoint.y, cp.x, cp.y);
                        chartView.updateSelection(mouse);

                        chartView.selectedPoint = cp;
                    }
                }

                onReleased: (mouse) => {
                    chartView.selectedPoint = undefined;
                }
            }

            // example function that tries to find the clicked point or add a new one
            function setSelected(point : MouseEvent) {
                var cp = chartView.mapToValue(Qt.point(point.x,point.y));
                for(var i = 0; i < lineSeries.count; i++)
                {
                    var p = lineSeries.at(i);
                    if(Math.abs(cp.x - p.x) <= chartView.toleranceX && Math.abs(cp.y - p.y) <= chartView.toleranceY)
                    {
                        chartView.selectedPoint = p;
                        chartView.selectedIndex = i;
                        chartView.updateSelection(point);

                        return;
                    }
                }

                chartView.addPoint(point);
                chartView.updateSelection(point);
            }

            function updateSelection(point: MouseEvent) {
                var newPoint = chartView.mapToValue(Qt.point(point.x,point.y));
                lineSeriesSelected.removePoints(0,lineSeriesSelected.count);
                lineSeriesSelected.insert(0, newPoint.x, newPoint.y);
            }

            // example function that insert a point at the click point
            function addPoint(point: MouseEvent) {
                console.log("add point: " + point);
                var index = 0;
                var newPoint = chartView.mapToValue(Qt.point(point.x,point.y));

                for (var i = 0; i < lineSeries.count; ++i) {
                    if (lineSeries.at(i).x > newPoint.x) {
                        console.log("found point at: " + index);

                        lineSeries.insert(i, newPoint.x, newPoint.y);
                        chartView.selectedIndex = i;

                        break;
                    }
                }

            }
        }
    }
}
