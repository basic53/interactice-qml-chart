import QtQuick

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Interactive Chart Demo")

    InteractiveChart {
        anchors.fill: parent
    }
}
