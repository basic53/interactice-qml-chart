cmake_minimum_required(VERSION 3.16)

cmake_policy(SET CMP0099 NEW)

# TODO set application name
set(EXECUTABLE_NAME "InteractiveChart")

project(${EXECUTABLE_NAME} VERSION 0.1 LANGUAGES CXX)

set(CMAKE_AUTOMOC ON)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD 20)

find_package(Qt6 6.2 COMPONENTS Quick QuickControls2 Charts LinguistTools REQUIRED)

# define is unit tests are build or not. This value can be overridden in the
# config.cmake configuration file. This can be used i.e. to create builds with
# and without tests
set(MY_UNITTESTS TRUE)

# collect all source files in the main project
file(GLOB CPP_SOURCES src/*.cpp)
file(GLOB HPP_SOURCES src/*.h)

qt_add_executable(${EXECUTABLE_NAME}
    ${CPP_SOURCES} ${HPP_SOURCES}
    ${IMAGE_RESOURCES}
)

# group the QML sources. Subfolders must be grouped seperately!
file(GLOB QML_SOURCES RELATIVE ${CMAKE_SOURCE_DIR} qml/*.qml)
source_group("Qml Files" FILES ${QML_SOURCES})

qt_add_qml_module(${EXECUTABLE_NAME}
    URI ${EXECUTABLE_NAME}
    VERSION 1.0
    QML_FILES ${QML_SOURCES}    
)

set_target_properties(${EXECUTABLE_NAME} PROPERTIES
    MACOSX_BUNDLE FALSE
    WIN32_EXECUTABLE TRUE
)

target_include_directories(${EXECUTABLE_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/src)

target_link_libraries(${EXECUTABLE_NAME}
    PRIVATE Qt6::Quick
    PRIVATE Qt6::QuickControls2
    PRIVATE Qt6::Charts
)

install(TARGETS ${EXECUTABLE_NAME}
    BUNDLE DESTINATION .
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

