# Interactice QML Chart

## Content

Basic example how to draw a line chart with QML and interact with the displayed points.

The example provides point selection and point dragging.

## Remark

QML charts are only working in QApplication. QGuiApplication is not working, because charts uses QWidgets in background.

## License

There is no license. Just use it as you wish...
